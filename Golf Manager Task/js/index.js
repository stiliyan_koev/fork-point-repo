$(document).ready(function() {
    let myIndex = 0;
    $('#circle-0').addClass('hover');
    repositionSticky();

    if ($(window).width() < 1023) {
        makeNavMobile();
    }
    $(window).resize(function() {
        repositionSticky();
    })

    autoSlidePictures();
    $('.circle').on('click', function() {
        let $buttonPressed = $(this);
        changeImage($(this).val());
    });

    $('.bottom-nav').on({
        mouseenter: function() {
            hoverEffect(this, true);
        },
        mouseleave: function() {
            hoverEffect(this, false);
        }
    });
  
})

function autoSlidePictures() {
    if (myIndex > 2) {
        myIndex = 0;
    }
    let circleToFocus = '#circle-' + myIndex;
    $(circleToFocus).trigger('click');
    myIndex++;
    setTimeout(autoSlidePictures, 5000);
}
function changeImage(number) {
    if (number > 2) {
        number = 0;
    }
    $('.circle').removeClass('hover');
    $('#circle-' + number).addClass('hover');
    let images = [];
    images.push('images/golf-field.png');
    images.push('images/women-golfers.png');
    images.push('images/golf-field-with-players.png');
    number = parseInt(number);
    $('#image-holder').attr('src', images[number])
    number++;
}


function hoverEffect(item, hovered) {
    let type = item.getAttribute('data-icon-type');

    if (hovered) {
        hoveredSrc = type + '-icon-hovered.png';
        let icon = '#' + type + '-icon';
        $(icon).attr('src', 'images/' + type + '-icon-hovered.png');

        let itemText = '.' + type + '-text';
        $(itemText).css('color', '#5CA6F1');

    } else {
        hoveredSrc = type + '-icon.png';
        let icon = '#' + type + '-icon';
        $(icon).attr('src', 'images/' + type + '-icon.png');

        let itemText = '.' + type + '-text';
        $(itemText).css('color', '#3A3A3A');
    }
}

function repositionSticky() {
    if ($(window).width() > 768) {
        let totalWidth = $(window).width();
        let sideWidth = (totalWidth - 900) / 2;
        let toMove = 900 + sideWidth + 15;
        $('.sticky').css("left", toMove);
    } else {
        $('.sticky').appendTo('footer');
    }
}

function makeNavMobile() {
    $(".topnav").css({
        'height': ''
    });
    let $topNav = $('#respNav');

    if ($topNav.attr('class') === ('topnav')) {
        $topNav.addClass('responsive');
        $topNav.addClass('small-12');
    } else {
        $topNav.removeClass('responsive');
        $topNav.removeClass('small-12');
        $topNav.removeClass('large-12');
        $topNav.addClass('topnav');
    }
}