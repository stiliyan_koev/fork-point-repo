$(document).ready(function() {
    if ($(window).width() < 1023) {
        makeNavMobile();
    }

    addArrow();

    $(".menu").hover(
        function() {
            $(this).find('.arrow-place').toggleClass('down-arrow');
        }
    );
})

function addArrow(){
    $('li').addClass('grid-x grid-padding-x align-middle align-center');
    $('.down-arrow').removeClass('align-middle');
    $('.offer-first-li').removeClass('align-middle');
}
function makeNavMobile() {
    $(".topnav").css({
        'height': ''
    });
    let $topNav = $('#respNav');

    if ($topNav.attr('class') === 'topnav') {
        $topNav.addClass('responsive');
        $topNav.addClass('small-12');
    } else {
        $topNav.removeClass('responsive');
        $topNav.removeClass('small-12');
        $topNav.removeClass('large-12');
        $topNav.addClass('topnav');
    }
}