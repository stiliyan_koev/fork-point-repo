let googleMap;
let map;
let geocoder;
let markers = [];
let manualLocation = false;

function myMap() {
    googleMap = {
        center: new google.maps.LatLng(43.827980,25.971561),
        zoom: 10,
    };
    map = new google.maps.Map(document.getElementById("googleMap"),googleMap);
    geocoder = new google.maps.Geocoder();
    let latLng = map.getCenter();

    map.addListener('click', function (event) {
        if(manualLocation) {
            let clickedlatLng = event.latLng;
            deleteMarkers();

            geocodeLatLng(geocoder, map, clickedlatLng);
            addMarker(clickedlatLng);
        }
    });

    var input = document.getElementById('addressInput');
    var searchBox = new google.maps.places.SearchBox(input);
    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });

    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }

    });
    addMarker(latLng)
    map.panTo(latLng);
}
function geocodeAddress(geocoder, resultsMap,address) {
    deleteMarkers();
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
            addMarker(results[0].geometry.location);
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}
function placeMarker(latitude, longitude) {

    let uluru = {lat: latitude, lng: longitude};
    let marker = new google.maps.Marker({
        position: uluru,
        map: map
    });
    markers.push(marker);

}
function addMarker(location) {
    let marker = new google.maps.Marker({
        position: location,
        map: map
    });
    markers.push(marker);
}

function setMapOnAll(map) {
    for (let i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}
function clearMarkers() {
    setMapOnAll(null);
}
function showMarkers() {
    setMapOnAll(map);
}
function deleteMarkers() {
    clearMarkers();
    markers = [];
}
function geocodeLatLng(geocoder, map,latlng) {
    for (let i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    geocoder.geocode({'location': latlng}, function(results, status) {
        if (status === 'OK') {
            if (results[0]) {
                document.getElementById("addressInput").value = results[0].formatted_address;
            } else {
                window.alert('No results found');
            }
        } else {
            window.alert('Geocoder failed due to: ' + status);
        }
    });
}
function enableManualLocation(){
    manualLocation = true;
}