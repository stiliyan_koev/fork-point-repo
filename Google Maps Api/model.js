let name;
let address;
let email;
let phone;
let website;

function validateForm() {
    manualLocation = false;
    let result = validateName() && validateAddress() && validateEmail() && validatePhone() && validateWebsite();
    if(result) {
        saveUserInput();
        geocodeAddress(geocoder, map, address);
    }
    return false;
}
function validateName() {
    name = document.getElementById("nameInput").value;
    if(name == ""){
        alert("Name field must be filled out !")
        return false;
    }else{
        return true;
    }

}
function validateAddress() {
    address = document.getElementById("addressInput").value;
    if(address == ""){
        alert("Address field must be filled out !")
        return false;
    }else{
        return true;
    }
}
function validateEmail() {
    email = document.getElementById("emailInput").value;
    if(email == ""){
        alert("Email field must be filled out !")
        return false;
    }else{
        return true;
    }
}
function validatePhone() {
    phone = document.getElementById("phoneInput").value;
    if(phone == ""){
        alert("Phone field must be filled out !")
        return false;
    }else{
        return true;
    }
}
function validateWebsite() {
    website = document.getElementById("websiteInput").value;
    if (website == "") {
        alert("Website field must be filled out !")
        return false;
    } else {
        return true;
    }
}
function saveUserInput() {
    let userInput = {name: name,address: address,email: email,phone: phone,website: website};
    localStorage.setItem(name,JSON.stringify(userInput));
    console.log(JSON.parse(localStorage.getItem(0)));
}