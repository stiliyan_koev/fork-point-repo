var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongo = require('mongodb');

var indexRouter = require('./routes/index');
var categoriesRouter = require('./routes/categories');
var subCategoriesRouter = require('./routes/subcategories');
var productsRouter = require('./routes/products');
var productRouter = require('./routes/product');
var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/categories', categoriesRouter);
app.use('/subcategories', subCategoriesRouter);
app.use('/products', productsRouter);
app.use('/product', productRouter);

/*This is Soap API request for fetching the currency rates based on USD to latest updated date. */

var soap = require('soap');
var url = 'http://infovalutar.ro/curs.asmx?wsdl';
var data = new Date();
var d = JSON.stringify(data);
var currencies = {};
var latestDate = 0;


soap.createClient(url, function (err, client) {

  client.lastdateinserted(function (err, result) {
    latestDate = result.lastdateinsertedResult.toJSON();

    args = { dt: latestDate };
    client.getall(args, function (err, result) {

      result.getallResult.diffgram.DocumentElement.Currency.forEach(element => {
        currencies[element.IDMoneda] = element.Value;
      });
      app.locals.currencies = currencies;
    });
  });
});

/*This is REST API request for fetching the currency rates based on USD to latest updated date. */
var usdRatesFromREST= {toBGN: 0, toEUR: 0};
const axios = require("axios");
const restURL =
  "http://www.apilayer.net/api/live?access_key=386c198468585673e04830eec1f6206f&base=USD&symbols=BGN,EUR";
axios
  .get(restURL)
  .then(response => {
    usdRatesFromREST.toBGN = response.data.quotes['USDBGN'];
    usdRatesFromREST.toEUR = response.data.quotes['USDEUR'];
    app.locals.usdRatesFromREST = usdRatesFromREST;
  })
  .catch(error => {
    console.log(error);
  });

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function (err, database) {
  if (err) throw err;
  var dataBaseObject = database.db("taskDb");
  app.locals.dbo = dataBaseObject;
});


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;