/**
* Description of the Controller and the logic it provides
*
* @module  controllers/Six
*/

'use strict';

// HINT: do not put all require statements at the top of the file
// unless you really need them for all functions

/**
* Description of the function
*
* @return {String} The string 'myFunction'
*/
// var myFunction = function(){
//     return 'myFunction';
// }

/* Exports of the controller */
///**
// * @see {@link module:controllers/Six~myFunction} */
//exports.MyFunction = myFunction;

var URLUtils = require('dw/web/URLUtils');
var ISML = require('dw/template/ISML');
var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var app = require('app_storefront_controllers/cartridge/scripts/app');

function start(){
	app.getForm('newsletter').clear();

	/* use the app module to do display the form as follows(ICN node equivalent)
	1. Process the submit button named “subscribe”
	2. The controller to handle form submission will be “Newsletter”
	3. The function to handle form submission should be “HandleForm”
	4. The form to be rendered is “newslettersignup” */
	
	app.getView({
		Action: 'subscribe',
		ContinueURL: URLUtils.https('Newsletter-HandleForm')
	}).render('newsletter/newslettersignup');
	
	
}

function handleForm(){
	Address = app.getModel('Address');
	var newsletterForm = app.getForm('newsletter');
	response.getWriter().println('Hello World from pipeline controllers !' + newsletterForm.object.fname.value);
    
    newsletterForm.handleAction({
		
		//inline function to process form
		submit: function(){
			response.redirect(URLUtils.https('newslettersuccess',{CurrentForms: newsletterForm}));
			return;
		}
		/* if subscribe button is successfully pressed then display
		 “newsletter/newslettersuccess.isml” and pass CurrentForms on pdict
		 Add return statement at the end.
		 */
		
    });
    //response.redirect(URLUtils.https('Newsletter-Start'));
}
exports.Start = guard.ensure(['get'], start);
exports.HandleForm = guard.ensure([], handleForm);