$(function () {
    wholeDb.forEach(function (category) {
        <div class="col-sm-4">
            <a href="/categories?subcategory= category.id">
                <button type="button" class="btn btn-primary button-categories">
                    category.name
            </button>
            </a>
        </div>
    });
)};