let express = require('express');
let router = express.Router();

router.get('/', function (req, res, next) {
    let subcategory = req.query.subcategory;
    var url = 'mongodb://localhost:27017/';
    var db = req.app.locals.dbo;

    var mongo = require('mongodb');
    var wholeDb = {};
    db.collection('categories').find().toArray(function (err, result) {
        wholeDb = result;
        if (err) throw err;

        db.collection('categories').find({ id: subcategory }).toArray(function (err, subcategoryCategoryResult) {
            if (err) throw err;
            res.render('categories', {
                title: 'Categories',
                mainCategory: subcategoryCategoryResult,
                subcategory: subcategory,
                wholeDb: wholeDb
            });
        });
    });
});


module.exports = router;