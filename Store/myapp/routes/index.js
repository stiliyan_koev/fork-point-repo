var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
    // res.redirect('categories?subcategory=Mens');
    res.render('index', {
        title: "Shop"
    });
});

module.exports = router;
