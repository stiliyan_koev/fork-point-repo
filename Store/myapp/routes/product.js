var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {

    var item_id = req.query.item_id;
    var db = req.app.locals.dbo;
    var currencies = req.app.locals.currencies;
    var parentCategory = req.query.parent_category;
    var subCategory = req.query.subCategory;
    var productGroup = req.query.product_group
    var usdRatesFromREST = req.app.locals.usdRatesFromREST;
    var parentCategoryCapitalized = parentCategory.charAt(0).toUpperCase() + parentCategory.slice(1);

    db.collection('products').find({ id: item_id }).toArray(function (err, result) {
        if (err) throw err;
        var prices = soapService(result[0].price, currencies);
        res.render('product', {
            data: result[0],
            parentCategory: parentCategory,
            parentCategoryCapitalized: parentCategoryCapitalized,
            subCategory: subCategory,
            productGroup: productGroup,
            priceInUSD: prices.USD,
            priceInBGN: prices.BGN,
            priceInEUR: prices.EUR
        });
    });
});


module.exports = router;

function soapService(priceInUSD, currencies) {
    var courseToBGN = parseFloat(currencies["BGN"]);
    var courseToEUR = parseFloat(currencies["EUR"]);
    var courseToUSD = parseFloat(currencies["USD"]);
    var currencies = { USD: 0, EUR: 0, BGN: 0 };

    var priceInLEI = priceInUSD * courseToUSD;

    currencies.USD = Number(priceInUSD).toFixed(2) + ' $'
    currencies.EUR = Number(priceInLEI / courseToEUR).toFixed(2) + ' €';
    currencies.BGN = Number(priceInLEI / courseToBGN).toFixed(2) + ' лв';
    return currencies;
}

function restService(priceInUSD, currencies) {

    var toBGN = parseFloat(usdRatesFromREST.toBGN);
    var toEUR = praseFloat(usdRatesFromREST.toEUR);

    currencies.EUR = priceInUSD * toBGN;
    currencies.BGN = priceInUSD * toEUR;
    currencies.USD = priceInUSD;

    return currencies;
}

