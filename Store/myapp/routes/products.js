var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
    var perPage = 6;
    var page = req.query.page || 1;
    var db = req.app.locals.dbo;
    var id = req.query.product_id;
    var parentCategory = req.query.parent_category;
    var subCategory = req.query.subCategory;
    var productId = req.query.product_id;
    var count = 0;
    console.log('page: ' + page);

    db.collection('products').find({
            primary_category_id: id
        })
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .toArray(function (err, result) {
            db.collection('products').find({
                primary_category_id: id
            }).toArray(function (err, allItems) {
                res.render('products', {
                    title: id,
                    data: result,
                    id: id,
                    parentCategory: parentCategory,
                    subCategory: subCategory,
                    productId: productId,
                    products: result,
                    currentPage: parseInt(page),
                    numberOfPages: Math.ceil(allItems.length / perPage)
                })
            })
        })
})


module.exports = router;
