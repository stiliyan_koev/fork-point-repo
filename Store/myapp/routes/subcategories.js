var express = require('express');
var router = express.Router();


router.get('/', function (req, res, next) {
    var currentSubcategory = req.query.subcategory;
    var parentCategory = req.query.parent_category_id;
    var parentCategoryCapitalized = parentCategory.charAt(0).toUpperCase() + parentCategory.slice(1);


    var db = req.app.locals.dbo;
    db.collection('categories').find({ id: parentCategory }).toArray(function (err, result) {
        if (err) throw err;
        res.render('subcategories', {
            title: currentSubcategory,
            data: result, currentSubcategory: currentSubcategory,
            parentCategory: parentCategoryCapitalized,
            parentCategory: parentCategory
        });

    });
});

module.exports = router;
