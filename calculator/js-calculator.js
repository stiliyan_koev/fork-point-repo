$(function () {
    let input = "";
    let dotAllowed = true;
    let numberBeforeDot = false;
    let lastSymbolOperandPattern = "[-|+|*|//](?=$)";

    $('button').on('click',function () {
        let $buttonPressed = $(this);
        if($buttonPressed.hasClass("number")){
            numberBeingPressed($buttonPressed);
        }else if($buttonPressed.hasClass("operand")){
            operandBeingPressed($buttonPressed);
        } else if($buttonPressed.hasClass("equal")){
            equalBeingPressed();
        } else if($buttonPressed.hasClass("clean")) {
            cleanBeingPressed();
        } else if($buttonPressed.hasClass("dot")){
            dotBeingPressed();
        }else if($buttonPressed.hasClass("erase")){
            eraseBeingPressed();
        }
    });

    function numberBeingPressed($button) {
        input +=$button.val();
        showInput(input)
        numberBeforeDot = true;
    }

    function operandBeingPressed($button) {
        dotAllowed = true;
        numberBeforeDot = false;
        let isLastAnOperand = input.match(lastSymbolOperandPattern,input);
        let lastSymbol = input.substr(-1);
        if(lastSymbol=="."){
            input += "0" + $button.val();
        } else if (isLastAnOperand != null) {
            changeOperand($button);
        }else{
            input += $button.val();
        }
        showInput(input);
    }

    function changeOperand($button) {
        input = input.slice(0,-1);
        input += $button.val();
    }

    function equalBeingPressed(){
        let sum = eval(input);
        showInput(sum);
        input = sum.toString();
    }

    function cleanBeingPressed() {
        dotAllowed = true;
        numberBeforeDot=false;
        input = "";
        showInput(input);
    }

    function  dotBeingPressed() {
        let lastSymbol = input.substr(-1);

        if(dotAllowed) {
            if(!numberBeforeDot) {
            input += "0.";

            } else {
                input += ".";
            }
            showInput(input);
            dotAllowed = false;
        }
    }
    function eraseBeingPressed() {
        let last = input.substr(-1);
        let isOperand = input.match(lastSymbolOperandPattern,input);
        if(operand != null){
            dotAllowed = false;
        }
        if(last =="."){
            dotAllowed = true;
        }
        input = input.slice(0,-1);
        showInput(input);
    }

    function showInput(text) {
        $("#input").val(text);
    }
})