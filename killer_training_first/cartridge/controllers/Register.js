'use strict';

var guard = require('app_storefront_controllers/cartridge/scripts/guard');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Transaction = require('dw/system/Transaction');
var app = require('app_storefront_controllers/cartridge/scripts/app');
var res = require('app_storefront_controllers/cartridge/scripts/util/Response');
var Resource = require('dw/web/Resource');
var PagingModel = require('dw/web/PagingModel');

/**
 * Method "Start" for controller "Register". It displays a welcome page.
 * @returns - rendered page as response.
 */
function start() {
	app.getView().render('registration/registrationwelcome');
}
/**
 * Method "Start" for controller "Register". It redirects to the registration page containing registration form. On submit, Register-HandleForm is invoked.
 * @returns - Response for valid or invalid submitting.
 */
function register() {
    app.getView({
    	continueURL: dw.web.URLUtils.url('Register-HandleForm')
    }).render('registration/registration');
}

/**
 * Handles the registration form. After successful submit, custom object is created to store the new account's data. 
 * It is stored in Custom Object "Profile". Then it clears the form from the previous input.
 * 
 * @returns - Same page, but with cleared form.
 */
function handleForm() {
    var TriggeredAction = request.triggeredFormAction;
    var isSuccessful = false;
    var toRender = "";
    var CurrentHttpParameterMap = request.httpParameterMap;
    var firstName = "";
    var lastName = "";
    var message = Resource.msg('email.error', 'task', null);
    
    if (TriggeredAction != null) {
        if (TriggeredAction.formId == 'register') {
            var registerForm = app.getForm('register').object;
            var allAccounts = CustomObjectMgr.queryCustomObjects("Profile", "", "custom.email asc");
            firstName = registerForm.fname.htmlValue;
            lastName = registerForm.fname.htmlValue;
            isSuccessful = createCustomObject(registerForm);
            registerForm.clearFormElement();
        }
    }
    
    if (isSuccessful) {
    	app.getView({
    		firstName: firstName,
    		lastName: lastName
    	}).render('registration/registrationsuccess');
    } else {
    	res.renderJSON({error: true, message: message});
    }
}

/**
 * Method "Display" for controller "Register". It displays all registrations created on a new page.
 * @returns - Renders page for displaying all the registrations.
 */
function display() {
	var allAccounts = CustomObjectMgr.queryCustomObjects("Profile", "", "creationDate asc");
    var parameterMap = request.httpParameterMap;
    var pageSize = parameterMap.sz.intValue || 5;
    var start = parameterMap.start.intValue || 0;
    var end = start + pageSize;
    var profilePagingModel = createPagingModel(pageSize, start, allAccounts);
 
  
    app.getView({
    	ProfilePagingModel: profilePagingModel
    }).render('registration/registrationdisplay');
}
/**
 * Creating PagingModel.
 * @param pageSize - Number of records at one page.
 * @param start - Identifier for from which position the loop should start accessing.
 * @param allAccounts - Iterator with all data from Custom Object.
 * @returns - Instance of PagingModel.
 */
function createPagingModel(pageSize, start, allAccounts) {
	 var profilePagingModel = new PagingModel(allAccounts, allAccounts.count);
	 profilePagingModel.setPageSize(pageSize);
	 profilePagingModel.setStart(start);
	  
	 return profilePagingModel;
}

/**
 * Creating custom object for storing information from register form.
 * @param registerForm - A form containing all the information to be stopred.
 */
function createCustomObject(registerForm) {
	var result = false;
    Transaction.wrap(function () {
    	try {
    		 var customObject = CustomObjectMgr.createCustomObject("Profile", registerForm.email.value);
        		registerForm.copyTo(customObject);
        		result = true;
    	} catch(err) {
    		result = false;
    		dw.system.Logger.error(err);
    	}
       
    });
    
    return result;
}

/**
 * Sorting all the registrations in page Display by given parameter and option ascending/descending .
 * @returns - Rendered page with sorted records.
 */
function sort() {
	var parameterMap = request.httpParameterMap;
    var sortBy = parameterMap.sortBy.stringValue;
    
    if(sortBy!='creationDate asc') {
    	sortBy = 'custom.' + sortBy;
    }
    var allAccounts = CustomObjectMgr.queryCustomObjects("Profile", "", sortBy);
    var pageSize = parameterMap.sz.intValue || 5;
    var start = parameterMap.start.intValue || 0;
    
    var profilePagingModel = createPagingModel(pageSize, start, allAccounts);
    
    app.getView({
    	ProfilePagingModel: profilePagingModel,
    }).render('registration/registrationdisplaysorted');
}

exports.Start = guard.ensure(['get'], start);
exports.Register = guard.ensure(['get'], register);
exports.HandleForm = guard.ensure(['post'], handleForm);
exports.Display = guard.ensure(['get', 'post'], display);
exports.Sort = guard.ensure(['post'], sort);