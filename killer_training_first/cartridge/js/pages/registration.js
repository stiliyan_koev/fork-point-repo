'use strict';
var currentSortByOption = 'creationDate asc';

exports.init = function () {
    highlightSection();
    initRegistrationForm();
};
/**
 * Add event handlers to elements.
 */
function initRegistrationForm() {
    $(document).on('click', '.cb_hide', function () {
        if ((this).checked == true) {
            $(this).closest('tr').find('.content').fadeOut('slow');
        } else {
            $(this).closest('tr').find('.content').fadeIn('slow');
        }
    });

    $('#sortBy').on('change', function (e) {
    	currentSortByOption = this.value;
    	getSortedTable(this.value);
    	
    });
    
    $(document).on('submit','form', function (e) {
    	e.preventDefault(e);
    	submitForm();
    });
    
    $('li > a').on('click', function(e) {
    	e.preventDefault();
    	changePage(this);
    });
}

/**
 * Calling Ajax request to the back-end for performing the sort.
 * @param type - A type to for the records be sorted by.
 * @returns - Rendered page from resposne recieved.
 */
function getSortedTable(type) {
	    $.ajax({
	        method: "POST",
	        url: Urls.registersort,
	        data: {
	            sortBy: type
	        },
	        success: function (response) {
	        	 $('.accounts').replaceWith(response);
	        }
	    });
}

/**
 * Preventing default submission and manipulates the result. Depending if the email is already in used, the form is not submitted and 
 * displays message for email used. If the form is valid and no errors occured it displays 'thank you' message.
 */
function submitForm() {
	var action = $('#submit_btn').attr('name');
	
	 $.ajax({
	        type: "POST",
	        url: Urls.registerhandleform,
	        data: $('.register-form').serialize() + '&' + action + '=x',
	        success: function (response) {
	        	if (response.error) {
	        		$('.error_email').text(response.message);
	        		$('.error_email').show();
	        	} else {
	        		$('.register-form').replaceWith(response.message);
	        	}
	        }
	    });
}

/**
 * Highlights the current section's tab by adding 'active' CSS class.
 */
function highlightSection() {
    var url = window.location.href;
    $(".section").each(function () {
        if (url == (this.href)) {
            $(this).addClass("current");
        }
    });
}

/**
 * Changing the page with records of the table.
 * @param element - A page number being clicked.
 * @returns - A response of full page, but after filtering it takes only the table and replace it.
 */
function changePage(element) {
	$.ajax({
	        method: "POST",
	        url: $(element).attr('href'),
	        data: {
	            sortBy: currentSortByOption
	        },
	        success: function (response) {
	        	 $('.accounts').replaceWith($(response).find('table'));
	        	 $('.pagination').replaceWith($(response).find('.pagination'));	 
	        }
	    });
}